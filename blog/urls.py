from django.conf.urls import patterns, url, include
from django.views.generic import dates
from views import PostDayArchiveView, PostMonthArchiveView, PostYearArchiveView, PostListView
from models import Post
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('blog.views',
    url(r'^admin/',
        view=include(admin.site.urls),
        name='admin_page'
    ),
    url(r'^(?P<year>\d{4})/(?P<month>\w{3})/(?P<day>\d{1,2})/(?P<slug>[-\w]+)/$',
        dates.DateDetailView.as_view(
            date_field='publish',
            model=Post,
            context_object_name='blog_detail'
        ),
        name='blog_detail'
    ),
    url(r'^(?P<year>\d{4})/(?P<month>\w{3})/(?P<day>\d{1,2})/$',
        view=PostDayArchiveView.as_view(),
        name='blog_archive_day'
    ),
    url(r'^(?P<year>\d{4})/(?P<month>\w{3})/$',
        view=PostMonthArchiveView.as_view(),
        name='blog_archive_month'
    ),
    url(r'^(?P<year>\d{4})/$',
        view=PostYearArchiveView.as_view(),
        name='blog_archive_year'
    ),
    url(r'^categories/(?P<slug>[-\w]+)/$',
        view='category_detail',
        name='blog_category_detail'
    ),
    url (r'^categories/$',
        view='category_list',
        name='blog_category_list'
    ),
    url(r'^tags/(?P<slug>[-\w]+)/$',
        view='tag_detail',
        name='blog_tag_detail'
    ),
    url (r'^search/$',
        view='search',
        name='blog_search'
    ),
    url (r'^comments/',
        include('django.contrib.comments.urls')
    ),
    url(r'^page/(?P<page>\d+)/$',
        view=PostListView.as_view(),
        name='blog_index_paginated'
    ),
    url(r'^$',
        view=PostListView.as_view(),
        name='blog_index'
    ),
)
